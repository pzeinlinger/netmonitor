#!/usr/bin/env python
import re
import time
import sys

rx_bytes = -1
tx_bytes = -1


def sizeof_fmt(num, suffix="B"):
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, "Yi", suffix)


if __name__ == "__main__":
    print("Network activity during the last 5 seconds")
    interface_name = sys.argv[1] if len(sys.argv) > 1 else None
    while 1:
        with open("/proc/net/dev", "r") as f:
            available_interfaces = []
            for line in f:
                columns = re.findall(r"([^\s:]+)", line)
                available_interfaces.append(columns[0])
                if columns and interface_name == columns[0]:
                    delta_rx = int(columns[1]) - rx_bytes if rx_bytes >= 0 else 0
                    delta_tx = int(columns[9]) - tx_bytes if rx_bytes >= 0 else 0
                    rx_bytes = int(columns[1])
                    tx_bytes = int(columns[9])
                    print(
                        " rx: {}   \ttx: {}  ".format(
                            sizeof_fmt(delta_rx), sizeof_fmt(delta_tx)
                        ),
                        end="\r",
                    )
                    time.sleep(5)
                    break
            else:
                print("Interface not found. Provide a valid interface as argument.")
                print("Available interfaces: {}".format(available_interfaces[2:]))
                sys.exit(1)
